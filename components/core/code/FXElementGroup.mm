/**
This file is part of the Volta project.
Copyright (C) 2007-2021 Kai Berk Oezer
https://robo.fish/wiki/index.php?title=Volta
https://gitlab.com/Kai.Oezer/Volta

Volta is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#import "FXElementGroup.h"
#import "FXElement.h"


NSString* FXPasteboardDataTypeElementGroup = @"robo.fish.volta.library.element-group";

@interface FXElementGroup ()
@property (nonatomic, copy) NSString* name;
@property (nonatomic) NSMutableArray<FXElement*> * elements;
@end


@implementation FXElementGroup
{
@private
  NSMutableArray<FXElement*> * _elements;
  NSString* _name;
}

@synthesize elements = _elements;
@synthesize name = _name;

- (id) initWithElementGroup:(VoltaPTElementGroup)elementGroup
{
  self = [super init];
  _elements = [[NSMutableArray alloc] initWithCapacity:elementGroup.elements.size()];
  _name = [[NSString alloc] initWithString:(__bridge NSString*)elementGroup.name.cfString()];
  for ( VoltaPTElement element : elementGroup.elements )
  {
    FXElement* elementWrapper = [[FXElement alloc] initWithElement:element];
    [_elements addObject:elementWrapper];
    FXRelease(elementWrapper)
  }
  return self;
}

- (void) dealloc
{
  FXRelease(_elements)
  FXRelease(_name)
  FXDeallocSuper
}


- (NSString*) description
{
  return [NSString stringWithFormat:@"%@ 0x%qx %@\n%@>", [self className], (unsigned long long)self, [self name], [self elements]];
}


#pragma mark NSCoding


- (void) encodeWithCoder:(NSCoder*)encoder
{
  [encoder encodeObject:_elements forKey:@"elements"];
  [encoder encodeObject:_name forKey:@"name"];
}


- (id) initWithCoder:(NSCoder*)decoder
{
	self = [super init];
	_name = [decoder decodeObjectOfClass:[NSString class] forKey:@"name"];
	FXRetain(_name)
	_elements = [decoder decodeObjectOfClass:[NSMutableArray<FXElement*> class] forKey:@"elements"];
	FXRetain(_elements)
	return self;
}

+ (BOOL) supportsSecureCoding { return YES; }

#pragma mark NSCopying


- (id) copyWithZone:(NSZone*)zone
{
	FXElementGroup* copy = [[[self class] alloc] init];
	copy.name = self.name;
	copy.elements = self.elements;
	return copy;
}


#pragma mark NSPasteboardWriting


- (id) pasteboardPropertyListForType:(NSString*)type
{
	return [NSKeyedArchiver archivedDataWithRootObject:self requiringSecureCoding:NO error:nil];
}


- (NSArray*) writableTypesForPasteboard:(NSPasteboard *)pasteboard
{
	return @[FXPasteboardDataTypeElementGroup];
}


- (NSPasteboardWritingOptions) writingOptionsForType:(NSString*)type pasteboard:(NSPasteboard*)pasteboard
{
	return NSPasteboardWritingPromised;
}


#pragma mark NSPasteboardReading


+ (NSArray*) readableTypesForPasteboard:(NSPasteboard*)pasteboard
{
	return @[FXPasteboardDataTypeElementGroup];
}


+ (NSPasteboardReadingOptions) readingOptionsForType:(NSString*)type pasteboard:(NSPasteboard*)pasteboard
{
	return NSPasteboardReadingAsData;
}


- (id) initWithPasteboardPropertyList:(id)data ofType:(NSString *)type
{
	FXRelease(self)
	NSError* decodingError = nil;
	self = [NSKeyedUnarchiver unarchivedObjectOfClass:[self class] fromData:data error:&decodingError];
	if (decodingError != nil)
	{
		DebugLog(@"decoding error : %@", [decodingError localizedDescription]);
	}
	FXRetain(self)
	return self;
}


@end

