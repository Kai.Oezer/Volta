/**
This file is part of the Volta project.
Copyright (C) 2007-2021 Kai Berk Oezer
https://robo.fish/wiki/index.php?title=Volta
https://gitlab.com/Kai.Oezer/Volta

Volta is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#import "FXModelGroup.h"
#import "FXModel.h"
#import <VoltaCore/VoltaLibraryProtocol.h>

NSString* FXVoltaLibraryPasteboardTypeModelGroup = @"robo.fish.volta.library.model-group";

@implementation FXModelGroup
{
@private
  NSMutableArray<FXMutableModel*> * _models;
  VoltaPTModelGroupPtr _group;
}

@synthesize models = _models;
//@synthesize group = mGroup; // FORBIDDEN because @synthesize and shared_ptr don't play together nicely.

- (id) initWithPersistentGroup:(VoltaPTModelGroupPtr)group library:(id<VoltaLibrary>)modelLibrary
{
  self = [super init];
  _group = group;
  _models = [[NSMutableArray alloc] initWithCapacity:group->models.size()];
  for( VoltaPTModelPtr & model : group->models )
  {
    FXMutableModel* modelWrapper = [[FXMutableModel alloc] initWithPersistentModel:model];
    [modelWrapper setLibrary:modelLibrary];
    [_models addObject:modelWrapper];
    FXRelease(modelWrapper)
  }
  return self;
}

- (void) dealloc
{
  FXRelease(mModels)
  FXDeallocSuper
}


#pragma NSObject overrides


- (BOOL) isEqual:(id)anObject
{
  if (anObject == self)
  {
    return YES;
  }
  if ((anObject == nil) || ![anObject isKindOfClass:[self class]])
  {
    return NO;
  }
  return _group == [(FXModelGroup*)anObject persistentGroup];
}

- (NSString*) description
{
  return [self name];
}

- (NSUInteger) hash
{
  return [[self name] hash];
}


#pragma mark Public


- (VoltaPTModelGroupPtr) persistentGroup
{
  return _group;
}

- (BOOL) isMutable
{
  NSAssert( _group.get() != nullptr, @"VoltaPTModelGroupPtr is empty." );
  if ( _group.get() != nullptr )
  {
    return _group->isMutable;
  }
  return NO;
}

- (NSString*) name
{
  NSAssert( _group.get() != nullptr, @"VoltaPTModelGroupPtr is empty." );
  if ( _group.get() != nullptr )
  {
    NSString* nameStr = (__bridge NSString*)_group->name.cfString();
    if ( nameStr != nil )
    {
      return [NSString stringWithString:nameStr];
    }
  }
  return nil;
}

- (void) setName:(NSString*)newName
{
  NSAssert( _group.get() != nullptr, @"VoltaPTModelGroupPtr is empty." );
  if ( _group.get() != nullptr )
  {
    _group->name = (__bridge CFStringRef)newName;
  }
}

- (void) setModels:(NSArray*)newModels
{
  [_models removeAllObjects];
  [_models addObjectsFromArray:newModels];
}


// MARK: NSSecureCoding


- (void) encodeWithCoder:(NSCoder*)encoder
{
  [encoder encodeObject:[self name] forKey:@"Name"];
  [encoder encodeObject:_models forKey:@"Models"];
  [encoder encodeBool:[self isMutable] forKey:@"IsMutable"];
  [encoder encodeInt64:(int64_t)(&_group) forKey:@"Group"]; // Note: Encoding the address of the smart pointer object
}

- (id) initWithCoder:(NSCoder*)decoder
{
  self = [super init];
  _models = [decoder decodeObjectOfClass:[NSMutableArray<FXMutableModel*> class] forKey:@"Models"];
  FXRetain(mModels)
  _group = *((VoltaPTModelGroupPtr*)[decoder decodeInt64ForKey:@"Group"]);
  [self setName:[decoder decodeObjectOfClass:[NSString class] forKey:@"Name"]]; // Important: Comes after mGroup is set.
  return self;
}

+ (BOOL) supportsSecureCoding { return YES; }

// MARK: NSPasteboardWriting


- (id) pasteboardPropertyListForType:(NSString*)type
{
  return [NSKeyedArchiver archivedDataWithRootObject:self requiringSecureCoding:NO error:nil];
}

- (NSArray*) writableTypesForPasteboard:(NSPasteboard *)pasteboard
{
  return @[FXVoltaLibraryPasteboardTypeModelGroup];
}

- (NSPasteboardWritingOptions) writingOptionsForType:(NSString*)type pasteboard:(NSPasteboard*)pasteboard
{
  return NSPasteboardWritingPromised;
}


#pragma mark NSPasteboardReading


+ (NSArray*) readableTypesForPasteboard:(NSPasteboard*)pasteboard
{
  return @[FXVoltaLibraryPasteboardTypeModelGroup];
}

+ (NSPasteboardReadingOptions) readingOptionsForType:(NSString*)type pasteboard:(NSPasteboard*)pasteboard
{
  return NSPasteboardReadingAsData;
}

- (id) initWithPasteboardPropertyList:(id)data ofType:(NSString *)type
{
	FXRelease(self)
	NSError* decodingError = nil;
	self = [NSKeyedUnarchiver unarchivedObjectOfClass:[self class] fromData:data error:&decodingError];
	if (decodingError != nil)
	{
		DebugLog(@"decoding error : %@", [decodingError localizedDescription]);
	}
	FXRetain(self)
	return self;
}


@end
