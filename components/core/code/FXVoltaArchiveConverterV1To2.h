/**
This file is part of the Volta project.
Copyright (C) 2007-2021 Kai Berk Oezer
https://robo.fish/wiki/index.php?title=Volta
https://gitlab.com/Kai.Oezer/Volta

Volta is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#import <tuple>

@interface FXVoltaArchiveConverterV1To2 : NSObject

/// @return a tuple containing the resulting element node, a list of warnings, and a list of errors, in that order.
/// The returned node can only be valid if there were no errors.
+ (std::tuple<FXXMLElementPtr, FXStringVector, FXStringVector>) convertRootElement:(FXXMLElementPtr)rootElement;

@end
