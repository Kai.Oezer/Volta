/**
This file is part of the Volta project.
Copyright (C) 2007-2021 Kai Berk Oezer
https://robo.fish/wiki/index.php?title=Volta
https://gitlab.com/Kai.Oezer/Volta

Volta is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#import "FXSchematicPaletteController.h"
#import "FXSchematicElement.h"
#import "VoltaSchematicElementGroup.h"
#import "FXSchematicPaletteElementsView.h"
#import "FXSchematicPaletteView.h"

static const CGFloat skPaletteHeight = 90.0;
static const CGFloat skElementsViewLeftMargin = 24;

@implementation FXSchematicPaletteController
{
@private  
  /// maps group names to FXSchematicElementGroup instances
  NSMutableArray* _elementGroups;

  /// the name of the current selected palette
  NSString* _selectedGroupName;

  BOOL _batchEditingElementGroups;

  NSMenu* _groupSelectionMenu;
  
  id<VoltaSchematicPaletteGroupEditor> __weak _groupEditor;
  FXSchematicPaletteElementsView* _elementsView;
  NSScrollView* _elementsScrollView;
  NSPopUpButton* _groupSelectionPopup;
  NSView* _scrollViewContainer;
}

@dynamic view;
@synthesize groupEditor = _groupEditor;


- (id) init
{
  self = [super init];
  _elementGroups = [[NSMutableArray alloc] init];
  _groupSelectionMenu = [[NSMenu alloc] initWithTitle:@"Palette Groups"];
  _batchEditingElementGroups = NO;
  return self;
}


- (void) dealloc
{
  [_groupSelectionMenu removeAllItems];
  FXRelease(_groupSelectionMenu)
  FXRelease(_elementGroups)
  FXRelease(_selectedGroupName)
  FXDeallocSuper
}


#pragma mark Public


- (void) setSelectedGroup:(NSString*)selectedGroupName
{
  [self setSelectedGroup:selectedGroupName animate:YES];
}


- (NSString*) selectedGroup
{
  return _batchEditingElementGroups ? _selectedGroupName : _elementsView.elements.name;
}


- (void) beginEditingElementGroups
{
  _batchEditingElementGroups = YES;
}


- (void) endEditingElementGroups
{
  _batchEditingElementGroups = NO;
  id<VoltaSchematicElementGroup> selectedGroup = nil;
  for ( id<VoltaSchematicElementGroup> group in _elementGroups )
  {
    if ( [group.name isEqualToString:_selectedGroupName] )
    {
      selectedGroup = group;
      break;
    }
  }
  if ( (selectedGroup == nil) && ([_elementGroups count] > 0) )
  {
    selectedGroup = _elementGroups[0];
  }
  [self rebuildGroupChooserMenu];
  [self setSelectedGroup:selectedGroup.name animate:NO];
}


#pragma mark NSResponder overrides


- (void) encodeRestorableStateWithCoder:(NSCoder*)state
{
  [super encodeRestorableStateWithCoder:state];
  NSString* displayedGroupName = _elementsView.elements.name;
  if ( displayedGroupName != nil )
  {
    [state encodeObject:displayedGroupName forKey:@"SchematicPalette_DisplayedGroup"];
  }
}


- (void) restoreStateWithCoder:(NSCoder*)state
{
  [super restoreStateWithCoder:state];
  NSString* displayedGroupName = [state decodeObjectForKey:@"SchematicPalette_DisplayedGroup"];
  if ( displayedGroupName != nil )
  {
    [self setSelectedGroup:displayedGroupName animate:NO];
  }
}


#pragma mark NSViewController overrides


- (void) loadView
{
  NSRect const dummyFrame = NSMakeRect(0, 0, 100, skPaletteHeight);
  NSView* view = [[FXSchematicPaletteView alloc] initWithFrame:dummyFrame];
  view.autoresizingMask = (NSViewHeightSizable | NSViewWidthSizable);
  [self setView:view];
  FXRelease(view)

  [self createElementsView];
  [self createGroupSelectionPopup];
  [self layOutViews];
  if ( [_elementGroups count] > 0 )
  {
    [self setSelectedGroup:[(id<VoltaSchematicElementGroup>)_elementGroups[0] name] animate:NO];
  }
}


#pragma mark VoltaSchematicPalette implementation


- (void) addElementGroup:(id<VoltaSchematicElementGroup>)group
{
	if ( ![_elementGroups containsObject:[group name]] )
	{
		[_elementGroups addObject:group];

		if ( !_batchEditingElementGroups && ([_elementGroups count] == 1) )
		{
			[_elementsView setElements:group animate:!_batchEditingElementGroups];
		}
	}
	else if ( !_batchEditingElementGroups && [[group name] isEqualToString:[[_elementsView elements] name]] )
	{
		// If the group is already shown in the elements viewer strip it may need a refresh.
		[_elementsView refresh];
	}
  if ( !_batchEditingElementGroups )
  {
    [self rebuildGroupChooserMenu];
  }
}


- (void) removeAllElementGroups
{
	[_elementGroups removeAllObjects];
	[_elementsView setElements:nil animate:NO];
	if ( !_batchEditingElementGroups )
	{
		[_elementsView refresh];
	}
  if ( !_batchEditingElementGroups )
  {
    [self rebuildGroupChooserMenu];
  }
}


- (CGFloat) minWidth
{
  return _elementsScrollView.frame.origin.x + 1;
}


#pragma mark Private


- (void) setSelectedGroup:(NSString*)groupName animate:(BOOL)animate
{
	if ( _batchEditingElementGroups )
	{
		if ( _selectedGroupName != groupName )
		{
			FXRelease(mSelectedGroupName)
			_selectedGroupName = [groupName copy];
		}
	}
	else
	{
		id<VoltaSchematicElementGroup> displayedGroup = nil;
		for ( id<VoltaSchematicElementGroup> group in _elementGroups )
		{
			if ( [[group name] isEqualToString:groupName] )
			{
				displayedGroup = group;
				break;
			}
		}
		if ( (displayedGroup == nil) && ([_elementGroups count] > 0) )
		{
			displayedGroup = _elementGroups[0];
		}
		if ( !_batchEditingElementGroups )
		{
			[_elementsView setElements:displayedGroup animate:animate];
		}
		[_groupSelectionPopup selectItemWithTitle:displayedGroup.name];
	}
  [[self.view window] invalidateRestorableState];
}


- (void) rebuildGroupChooserMenu
{
	[self->_groupSelectionMenu removeAllItems];
	if ( [self->_elementGroups count] > 0 )
	{
		for ( id<VoltaSchematicElementGroup> group in self->_elementGroups )
		{
			NSString* groupName = [group name];
			NSMenuItem* groupMenuItem = [[NSMenuItem alloc] initWithTitle:groupName action:@selector(selectGroup:) keyEquivalent:@""];
			[groupMenuItem setTarget:self];
			[self->_groupSelectionMenu addItem:groupMenuItem];
			FXRelease(groupMenuItem)
		}
		[self->_groupSelectionMenu addItem:[NSMenuItem separatorItem]];
	}
	NSMenuItem* editItem = [[NSMenuItem alloc] initWithTitle:FXLocalizedString(@"Edit…") action:@selector(selectGroup:) keyEquivalent:@""];
	[editItem setTarget:self];
	[editItem setRepresentedObject:self->_elementGroups]; // Retains mElementGroups. This prevents users from fouling up the app by creating a custom group named "Edit…"
	[self->_groupSelectionMenu addItem:editItem];
	FXRelease(editItem)
}


- (void) selectGroup:(id)sender
{
  NSAssert( [sender isKindOfClass:[NSMenuItem class]], @"Unexpected sender." );
	NSString* selectedGroupName = nil;
	if ( [(NSMenuItem*)sender representedObject] == _elementGroups )
	{
		[_groupEditor openGroupEditor];
	}
	else
	{
		selectedGroupName = [(NSMenuItem*)sender title];
	}
	[self setSelectedGroup:selectedGroupName animate:YES];
}


- (void) createElementsView
{
  NSRect const dummyFrame = NSMakeRect(skElementsViewLeftMargin, 0, 100, skPaletteHeight);
  _elementsView = [[FXSchematicPaletteElementsView alloc] initWithFrame:dummyFrame];

  _elementsScrollView = [[NSScrollView alloc] initWithFrame:dummyFrame];
  _elementsScrollView.hasHorizontalScroller = YES;
  _elementsScrollView.scrollerStyle = NSScrollerStyleOverlay;
  _elementsScrollView.scrollerKnobStyle = NSScrollerKnobStyleDark;
  _elementsScrollView.autohidesScrollers = YES;
  _elementsScrollView.drawsBackground = NO;
  _elementsScrollView.verticalScrollElasticity = NSScrollElasticityNone;
  _elementsScrollView.documentView = _elementsView;

  _scrollViewContainer = [[NSView alloc] initWithFrame:dummyFrame];
  [_scrollViewContainer addSubview:_elementsScrollView];
  [[self view] addSubview:_scrollViewContainer];

  FXRelease(mElementsView)
  FXRelease(mElementsScrollView)
  FXRelease(mScrollViewContainer)
}


- (void) createGroupSelectionPopup
{
  _groupSelectionPopup = [[NSPopUpButton alloc] initWithFrame:NSMakeRect(0, 0, 100, 12)];
  _groupSelectionPopup.pullsDown = NO;
  _groupSelectionPopup.bordered = NO;
  _groupSelectionPopup.preferredEdge = NSMaxXEdge;
  _groupSelectionPopup.action = @selector(selectGroup:);
  _groupSelectionPopup.target = self;
  _groupSelectionPopup.menu = _groupSelectionMenu;
  [[self view] addSubview:_groupSelectionPopup];
  FXRelease(mGroupSelectionPopup)
}


- (void) layOutViews
{
  // Note: There seems to be a bug in NSScrollView that requires an embedding view of the same size in order for the overlay scroll bars to work correctly.
  NSView* chooser = _groupSelectionPopup;
  NSView* scroller = _elementsScrollView;
  NSView* scrollerContainer = _scrollViewContainer;
  chooser.translatesAutoresizingMaskIntoConstraints = NO;
  scroller.translatesAutoresizingMaskIntoConstraints = NO;
  scrollerContainer.translatesAutoresizingMaskIntoConstraints = NO;
  NSDictionary* views = NSDictionaryOfVariableBindings(chooser, scroller, scrollerContainer);
  NSDictionary* metrics = @{
    @"paletteHeight"      : @(skPaletteHeight),
    @"elementsLeftMargin" : @(skElementsViewLeftMargin)
  };
  [scrollerContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"|[scroller]|" options:0 metrics:nil views:views]];
  [scrollerContainer addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[scroller]|" options:0 metrics:nil views:views]];
  NSArray* constraints = @[
    @"|-4-[chooser(10)]",
    @"|-elementsLeftMargin-[scrollerContainer]|",
    @"V:|[scrollerContainer]|",
    @"V:|-4-[chooser]"
  ];
  for ( NSString* constraint in constraints )
  {
    [[self view] addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:constraint options:0 metrics:metrics views:views]];
  }
}


@end
