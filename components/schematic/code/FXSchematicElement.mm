/**
This file is part of the Volta project.
Copyright (C) 2007-2021 Kai Berk Oezer
https://robo.fish/wiki/index.php?title=Volta
https://gitlab.com/Kai.Oezer/Volta

Volta is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#import "FXSchematicElement.h"
#import "VoltaSchematic.h"
#import <VoltaCore/VoltaLibraryProtocol.h>
#import "FXShape.h"
#import "FXVector.h"

NSString* VoltaSchematicElementUpdateNotification = @"VoltaSchematicElementUpdateNotification";
NSString* FXPasteboardDataTypeSchematicElement = @"robo.fish.volta.schematic.element";

@interface FXSchematicElement ()
@property (nonatomic) id<FXShape> shape;
@property NSDictionary* properties;
@end


@implementation FXSchematicElement
{
@private
  NSString*                 _name;
  NSString*                 _modelName;
  id<FXShape>               _shape;
  NSString*                 _modelVendor;
  CGFloat                   _rotation;
  BOOL                      _flipped;
  FXPoint                   _location;
  VoltaModelType            _type;
  SchematicRelativePosition _labelPosition;
  NSMutableDictionary*      _properties;               ///< model properties
  id<VoltaSchematic> __weak _schematic;
}

- (id) init
{
  if ( (self = [super init]) != nil )
  {
    _properties = [[NSMutableDictionary alloc] init];
    _location.x = 0.0f;
    _location.y = 0.0f;
    _rotation = 0.0f;
    _type = VMT_Unknown;
    _flipped = NO;
    _labelPosition = SchematicRelativePosition_None;
  }
  return self;
}

- (void) dealloc
{
  self.shape = nil;
  self.name = nil;
  self.modelName = nil;
  self.modelVendor = nil;
  FXRelease(mProperties)
  _properties = nil;
  FXDeallocSuper
}

@synthesize name = _name;
@synthesize modelName = _modelName;
@synthesize modelVendor = _modelVendor;
@synthesize shape = _shape;
@synthesize rotation = _rotation;
@synthesize flipped = _flipped;
@synthesize location = _location;
@synthesize type = _type;
@synthesize schematic = _schematic;
@synthesize labelPosition = _labelPosition;
@synthesize properties = _properties;


#pragma mark Public


- (void) setModelName:(NSString*)modelName
{
  if (modelName != _modelName)
  {
    FXRelease(mModelName)
    _modelName = [modelName copy];
    self.shape = nil;
  }
}


- (void) setModelVendor:(NSString*)modelVendor
{
  if (modelVendor != _modelVendor)
  {
    FXRelease(mModelVendor)
    _modelVendor = [modelVendor copy];
    self.shape = nil;
  }
}


- (NSUInteger) numberOfProperties
{
  return [_properties count];
}


- (void) enumeratePropertiesUsingBlock:(void (^)(NSString* key, id value, BOOL* stop))block
{
  [_properties enumerateKeysAndObjectsUsingBlock:^(id key, id value, BOOL* stop) {
    block((NSString*)key, value, stop);
  }];
}


- (id) propertyValueForKey:(NSString*)key
{
  return [_properties objectForKey:key];
}


- (void) setPropertyValue:(id)value forKey:(NSString*)key;
{
  if (key != nil)
  {
    if ( value == nil )
    {
      [_properties removeObjectForKey:key];
    }
    else
    {
      [_properties setObject:value forKey:key];
    }
  }
}


- (void) removeAllProperties
{
  [_properties removeAllObjects];
}


- (CGSize) size
{
  CGSize result = CGSizeZero;
  id<FXShape> shape = [self shape];
  if ( shape != nil )
  {
    FXVector v1( 0.0f, shape.size.height );
    FXVector v2( shape.size.width, 0.0f );
    v1.rotate( _rotation );
    v2.rotate( _rotation );
    result = CGSizeMake( abs(v1.x - v2.x), abs(v1.y - v2.y) );
  }
  return result;
}


- (CGRect) boundingBox
{
  CGRect result = CGRectZero;

  CGSize const shapeSize = self.shape.size;

  FXVector v1( -shapeSize.width/2.0, -shapeSize.height/2.0 );
  FXVector v2( shapeSize.width/2.0, -shapeSize.height/2.0 );
  FXVector v3( shapeSize.width/2.0, shapeSize.height/2.0 );
  FXVector v4( -shapeSize.width/2.0, shapeSize.height/2.0 );

  v1.rotate(_rotation);
  v2.rotate(_rotation);
  v3.rotate(_rotation);
  v4.rotate(_rotation);

  result.origin.x = MIN( v1.x, MIN( v2.x, MIN( v3.x, v4.x ) ) );
  result.origin.y = MIN( v1.y, MIN( v2.y, MIN( v3.y, v4.y ) ) );
  result.size.width = MAX( v1.x, MAX( v2.x, MAX( v3.x, v4.x ) ) ) - result.origin.x;
  result.size.height = MAX( v1.y, MAX( v2.y, MAX( v3.y, v4.y ) ) ) - result.origin.y;
  result.origin.x += _location.x;
  result.origin.y += _location.y;

  return result;
}


- (id<FXShape>) shape
{
  if ( _shape == nil )
  {
    _shape = [[_schematic library] shapeForModelType:_type name:_modelName vendor:_modelVendor];
    FXRetain(mShape)
  }
  return _shape;
}


- (void) prepareShapeForDrawing
{
  if ( self.shape.doesOwnDrawing )
  {
    self.shape.attributes = _properties;
  }
}


// MARK: NSObject overrides


- (BOOL) isEqual:(id)anObject
{
  if (anObject == self)
  {
    return YES;
  }
  if ((anObject == nil) || ![anObject isKindOfClass:[self class]])
  {
    return NO;
  }
  return [self isEqualToSchematicElement:anObject];
}


- (NSUInteger) hash
{
  // Note: We can't use [mName hash] because the name of an element can change.
  // The hash must be constant during the lifetime of the object.
  return [super hash];
}


- (NSString*) description
{
  return [NSString stringWithFormat:@"%p %@-%@-%@ at (%.0g,%.0g)", self, _modelVendor, _modelName, _name, _location.x, _location.y];
}


// MARK: NSSecureCoding


- (void) encodeWithCoder:(NSCoder*)encoder
{
  [encoder encodeObject:_name             forKey:@"Name"];
  [encoder encodeObject:_modelName        forKey:@"Model Name"];
  [encoder encodeObject:_modelVendor      forKey:@"Model Vendor"];
  [encoder encodeInteger:(NSInteger)_type forKey:@"Model Type"];
  [encoder encodeFloat:_rotation          forKey:@"Rotation"];
  [encoder encodePoint:_location          forKey:@"Location"];
  [encoder encodeObject:_properties       forKey:@"Model Properties"];
  [encoder encodeInteger:_labelPosition   forKey:@"Label Position"];
}


- (id) initWithCoder:(NSCoder*)decoder
{
  self = [super init];
  _name             = [decoder decodeObjectOfClass:[NSString class] forKey:@"Name"]; FXRetain(mName)
  _modelName        = [decoder decodeObjectOfClass:[NSString class] forKey:@"Model Name"]; FXRetain(mModelName)
  _modelVendor      = [decoder decodeObjectOfClass:[NSString class] forKey:@"Model Vendor"]; FXRetain(mModelVendor)
  _type             = (VoltaModelType)[decoder decodeIntegerForKey:@"Model Type"];
  _rotation         = [decoder decodeFloatForKey:@"Rotation"];
  _location         = [decoder decodePointForKey:@"Location"];
  _properties       = [decoder decodeObjectOfClass:[NSMutableDictionary class] forKey:@"Model Properties"]; FXRetain(mProperties)
  _labelPosition    = (SchematicRelativePosition)[decoder decodeIntegerForKey:@"Label Position"];
  _schematic = nil;
  return self;
}

+ (BOOL) supportsSecureCoding { return YES; }


// MARK: NSCopying


- (id) copyWithZone:(NSZone *)zone
{
  FX(FXSchematicElement)* newElement = [[[self class] allocWithZone:zone] init];
  [newElement setName: [self name]];
  [newElement setModelName: [self modelName]];
  [newElement setModelVendor: [self modelVendor]];
  [newElement setLocation: [self location]];
  [newElement setRotation: [self rotation]];
  [newElement setType: [self type]];
  NSMutableDictionary* copiedProperties = [[NSMutableDictionary alloc] initWithDictionary:self.properties copyItems:YES];
  [newElement setProperties: copiedProperties];
  FXRelease(copiedProperties)
  [newElement setLabelPosition: [self labelPosition]];
  [newElement setSchematic: [self schematic]];
  return newElement;
}



#pragma mark NSPasteboardWriting


- (id) pasteboardPropertyListForType:(NSString*)type
{
  return [NSKeyedArchiver archivedDataWithRootObject:self requiringSecureCoding:NO error:nil];
}


- (NSArray*) writableTypesForPasteboard:(NSPasteboard *)pasteboard
{
  return @[FXPasteboardDataTypeSchematicElement];
}


- (NSPasteboardWritingOptions) writingOptionsForType:(NSString*)type pasteboard:(NSPasteboard*)pasteboard
{
  return NSPasteboardWritingPromised;
}


#pragma mark NSPasteboardReading


+ (NSArray*) readableTypesForPasteboard:(NSPasteboard*)pasteboard
{
  return @[FXPasteboardDataTypeSchematicElement];
}


+ (NSPasteboardReadingOptions) readingOptionsForType:(NSString*)type pasteboard:(NSPasteboard*)pasteboard
{
  return NSPasteboardReadingAsData;
}


- (id) initWithPasteboardPropertyList:(id)data ofType:(NSString *)type
{
  FXRelease(self)
  NSError* decodingError = nil;
  self = [NSKeyedUnarchiver unarchivedObjectOfClass:[self class] fromData:data error:&decodingError];
  if (decodingError != nil)
  {
		DebugLog(@"decoding error : %@", [decodingError localizedDescription]);
	}
	FXRetain(self)
  return self;
}


#pragma mark Private


- (BOOL) isEqualToSchematicElement:(id<VoltaSchematicElement>)otherElement
{
  if ( self == otherElement )
  {
    return YES;
  }
  if ( _type != [otherElement type] )
  {
    return NO;
  }
  if ( ![_name isEqualToString:[otherElement name]] )
  {
    return NO;
  }
  return [_modelVendor isEqualToString:[otherElement modelVendor]];
}


@end
