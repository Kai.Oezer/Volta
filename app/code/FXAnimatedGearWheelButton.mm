/**
This file is part of the Volta project.
Copyright (C) 2007-2021 Kai Berk Oezer
https://robo.fish/wiki/index.php?title=Volta
https://gitlab.com/Kai.Oezer/Volta

Volta is free software. You can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#import "FXAnimatedGearWheelButton.h"
#include <math.h>
#include <atomic>

static const CGFloat skMaxRotationSpeed = M_PI_4/10.0;
static const NSUInteger skMaxTransitionCount = 24;
static const CGFloat skColor_disabled[] = { 0.45, 0.45, 0.45, 1.0 };
static const CGFloat skColor_stopped_fill[] = { 0.28, 0.28, 0.28, 1.0 };
static const CGFloat skColor_stopped_stroke[] = { 0.4, 0.4, 0.4, 1.0 };
static const CGFloat skColor_running_fill[] = { 0.7, 0.0, 0.0, 1.0 };
static const CGFloat skColor_running_stroke[] = { 0.6, 0.45, 0.45, 1.0 };


@implementation FXAnimatedGearWheelButton
{
@private
  BOOL               _userStartsClick;   // set to true on mouseDown and checked on mouseUp
  NSUInteger         _animationCounter;
  NSUInteger         _transitionCounter; // Used for gradual transition of colors.
  CGFloat            _rotation;
  std::atomic<int>   _animating;         // indicates that the animation is not running when its value is 0, and that the animation is running when the value is not 0.
  BOOL               _enabled;
  CGColorSpaceRef    _colorSpace;
  CGFloat const *    _fillColor;
  CGFloat const *    _strokeColor;
  CGFloat            _rotationSpeed;
  CGFloat            _color_transitional_fill[4];
  CGFloat            _color_transitional_stroke[4];
  dispatch_queue_t   _drawing_parameters_access;
}

- (id) initWithFrame:(NSRect)frame
{
  self = [super initWithFrame:frame];
  if (self)
  {
    _userStartsClick = NO;
    _animating = 0;
    _rotation = 0.0f;
    _rotationSpeed = skMaxRotationSpeed;
    _transitionCounter = 0;
    _enabled = NO;
    _colorSpace = CGColorSpaceCreateWithName(kCGColorSpaceGenericRGB);
    _drawing_parameters_access = dispatch_queue_create("gear wheel drawing parameters access", DISPATCH_QUEUE_SERIAL);
  }
  return self;
}


- (void) dealloc
{
  CGColorSpaceRelease(_colorSpace);
  FXDeallocSuper
}


#pragma mark Public


- (void) startAnimation
{

  if (!atomic_fetch_or(&_animating, 1))
  {
    _transitionCounter = skMaxTransitionCount;
    _rotationSpeed = skMaxRotationSpeed;
    [NSThread detachNewThreadSelector:@selector(animate) toTarget:self withObject:nil];
  }
}


- (void) stopAnimation
{
  _animating = 0;
}


- (BOOL) isAnimating
{
  return (_animating != 0);
}


- (BOOL) enabled
{
  return _enabled;
}


- (void) setEnabled:(BOOL)state
{
  _enabled = state;
  [self setNeedsDisplay:YES];
}


#pragma mark NSView overrides


- (void)drawRect:(NSRect)visibleRect
{
  static const CGFloat skStep = M_PI_4;
  static const CGFloat skRotationalDifference = 0.0576;
  static CGFloat const skMarginForStrokeWidth = 1.0;

  dispatch_sync(_drawing_parameters_access, ^{
		CGRect const rect = NSInsetRect([self frame], skMarginForStrokeWidth, skMarginForStrokeWidth);

		CGFloat const kDimension = MIN(rect.size.width,rect.size.height);
		CGFloat const kOuterRadius = kDimension * 0.28f;
		CGFloat const kInnerRadius = kDimension * 0.19f;
		CGFloat const kHoleRadius = kDimension * 0.105f;
		CGFloat centerX, centerY;
		CGFloat rotation;
		int i,j;

		CGContextRef context = FXGraphicsContext;

		[self updateColors];
		CGContextSetFillColorSpace(context, _colorSpace);
		CGContextSetFillColor(context, _fillColor);
		CGContextSetStrokeColorSpace(context, _colorSpace);
		CGContextSetStrokeColor(context, _strokeColor);

		// Turning each gear wheel
		for (i = -1; i < 2; i+=2)
		{
			const CGAffineTransform transform = CGAffineTransformIdentity;
			CGMutablePathRef path = CGPathCreateMutable();
			centerX = visibleRect.origin.x + (rect.size.width * 0.5f) + (i * 0.24f * kDimension);
			centerY = visibleRect.origin.y + (rect.size.height * 0.5f) + (i * 0.10f * kDimension);
			for (j = 0; j < (int)(2 * M_PI / skStep); j++)
			{
				rotation = i * _rotation + (i + 1) * skRotationalDifference + j * skStep; // wheels rotate in opposite directions
				CGPathAddArc(path, &transform, centerX, centerY, kInnerRadius, rotation, rotation + skStep * 0.4f, 0);
				// move outwards, creating a wheel projection
				CGPathAddLineToPoint(path, &transform, centerX + kOuterRadius * cos(rotation + skStep * 0.5f), centerY + kOuterRadius * sin(rotation + skStep * 0.5f));
				CGPathAddArc(path, &transform, centerX, centerY, kOuterRadius, rotation + skStep * 0.5f, rotation + skStep * 0.9f, 0);
				// move toward center, finishing the projection
				CGPathAddLineToPoint(path, &transform, centerX + kInnerRadius * cos(rotation + skStep), centerY + kInnerRadius * sin(rotation + skStep));
			}
			// move towards center to give the wheel a thickness but leave a hole at the center
			CGPathMoveToPoint(path, &transform, centerX + kInnerRadius * 0.5f, centerY);
			CGPathAddEllipseInRect(path, &transform, CGRectMake(centerX - kHoleRadius, centerY - kHoleRadius, 2.0f * kHoleRadius, 2.0f * kHoleRadius));
			CGPathCloseSubpath(path);
			CGContextAddPath(context, path);
			CGContextEOFillPath(context);
			CGContextFillPath(context);
			CGContextAddPath(context, path);
			CGContextStrokePath(context);
			CGPathRelease(path);
		}
	});
}

- (BOOL) isOpaque
{
  return NO;
}

- (BOOL) canDrawConcurrently
{
  return YES;
}

- (void) mouseDown:(NSEvent*)mouseEvent
{
  _userStartsClick = _enabled;
  [super mouseDown:mouseEvent];
}

- (void) mouseUp:(NSEvent*)mouseEvent
{
  if (_userStartsClick && _enabled)
  {
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Warc-performSelector-leaks"
    [self.target performSelector:self.action withObject:self];
  #pragma clang diagnostic pop
  }
  _userStartsClick = NO;
}

- (BOOL) acceptsFirstMouse:(NSEvent*)mouseEvent
{
  return YES; // click-through
}


#pragma mark NSResponder overrides


static NSString* FXResume_GearWheelButtonRotation = @"FXResume_GearWheelButtonRotation";


- (void) encodeRestorableStateWithCoder:(NSCoder*)coder
{
  [super encodeRestorableStateWithCoder:coder];
  [coder encodeFloat:_rotation forKey:FXResume_GearWheelButtonRotation];
}


- (void) restoreStateWithCoder:(NSCoder*)coder
{
  [super restoreStateWithCoder:coder];
  _rotation = [coder decodeFloatForKey:FXResume_GearWheelButtonRotation];
  [self setNeedsDisplay:YES];
}


#pragma mark Private


- (void) animate
{
  NSAssert( ![NSThread isMainThread], @"The animation must not run in the main thread" );
	@autoreleasepool
	{
		while ( (_animating != 0) || (_transitionCounter > 0) )
		{
			dispatch_sync(_drawing_parameters_access, ^{
				if ( _animating == 0 )
				{
					_rotationSpeed = _rotationSpeed/1.1;
				}
				_rotation += _rotationSpeed;
				if (_rotation > 2*M_PI)
				{
					_rotation -= 2*M_PI;
				}

				if ( _animating == 0 )
				{
					_transitionCounter -= 1;
				}
			});

			dispatch_async(dispatch_get_main_queue(), ^{
				[self setNeedsDisplay:YES];
			});
			[NSThread sleepUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.05]];
		}
	}
}


static void interpolateColor( CGFloat factor, CGFloat * outputColor, CGFloat const * inputColor1, CGFloat const * inputColor2 )
{
  if ( factor < 0.0 )
    factor = 0.0;
  if ( factor > 1.0 )
    factor = 1.0;
  outputColor[0] = inputColor1[0] + (factor * (inputColor2[0] - inputColor1[0]));
  outputColor[1] = inputColor1[1] + (factor * (inputColor2[1] - inputColor1[1]));
  outputColor[2] = inputColor1[2] + (factor * (inputColor2[2] - inputColor1[2]));
  outputColor[3] = inputColor1[3] + (factor * (inputColor2[3] - inputColor1[3]));
}


- (void) updateColors
{
  if ( (_transitionCounter > 0) && (_transitionCounter < skMaxTransitionCount) )
  {
    CGFloat const factor = ((CGFloat)_transitionCounter) / skMaxTransitionCount;
    interpolateColor(factor, _color_transitional_fill, skColor_stopped_fill, skColor_running_fill);
    interpolateColor(factor, _color_transitional_stroke, skColor_stopped_stroke, skColor_running_stroke);
    _fillColor = _color_transitional_fill;
    _strokeColor = _color_transitional_stroke;
  }
  else
  {
    _fillColor = ( _enabled ? ( _animating ? skColor_running_fill : skColor_stopped_fill ) : skColor_disabled );
    _strokeColor = ( _enabled ? ( _animating ? skColor_running_stroke : skColor_stopped_stroke ) : skColor_disabled );
  }
}


@end
